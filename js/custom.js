$(window).load(function() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        $('body').addClass('ios');
    } else {
        $('body').addClass('web');
    };
    

    //check styles
    Modernizr.addTest('backgroundclip', function() {
        var div = document.createElement('div');
        if ('backgroundClip' in div.style)
            return true;
        'Webkit Moz O ms Khtml'.replace(/([A-Za-z]*)/g, function(val) {
            if (val + 'BackgroundClip' in div.style) return true;
        });
    });

});

/* viewport width */
function viewport() {
    var e = window,
        a = 'inner';
    if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return {
        width: e[a + 'Width'],
        height: e[a + 'Height']
    }
};
/* viewport width */
$(function() {



    // SLIDERS 
    if ($('.js-slider').length) {
        if ($('.slider-instagram').length) {
            $('.slider-instagram').owlCarousel({
                items: 5,
                loop: true,
                margin: 17,
                responsive : {
                0 : {
                    items : 1,
                    dots: true
                },
                380 : {
                    items : 2,
                    nav: true,
                    dots: true
                },
                690 : {
                    items : 3,
                    nav: true,
                    dots: false
                },
                890 : {
                    items : 4,
                    nav: true,
                    dots: false
                },
                1024 : {
                    items : 5,
                    nav: true,
                    dots: false
                }
            }
            });
        }    
        if ($('.certificate-slide').length) {
            $('.certificate-slide').slick({
                dots: true,
                arrows: false,
                infinite: true,
                speed: 300,
                cssEase: 'linear',
                slidesToShow: 1,
                slidesToScroll: 1,
            });
        }
        if ($('.slider-info').length) {
            $('.slider-info').slick({
                dots: false,
                arrows: false,
                infinite: true,
                swipe: false,
                fade: true,
                speed: 300,
                cssEase: 'linear',
                slidesToShow: 1,
                slidesToScroll: 1,
                draggable: false,
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        adaptiveHeight: true
                    }
                }]
            });
        }
        if ($('.slider-instagram').length) {
            $('.info-illustrations__slider').each(function() {
                var el = $(this);
                el.slick({
                    dots: false,
                    arrows: true,
                    infinite: true,
                    swipe: true,
                    speed: 300,
                    cssEase: 'linear',
                    slidesToShow: 1,
                    slidesToScroll: 1,
                })
            });
        }

        if ($('.slider-promo').length) {
            var $numberReview = $('.slider-promo__pages .number-review-slide');
            var $slickReview = $('.slider-promo');
            $slickReview.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
                var i = (currentSlide ? currentSlide : 0) + 1;
                $numberReview.html('<span class="curr-number">' + i + '</span> <span class="curr-number_arr"> / </span> ' + 4);
            });

            $('.slider-promo').slick({
                dots: false,
                infinite: true,
                speed: 300,
                fade: true,
                cssEase: 'linear',
                slidesToShow: 1,
                slidesToScroll: 1,
            });
        };
        if ($('.slider-location').length) {
            $('.slider-location').slick({
                dots: false,
                arrows: false,
                infinite: true,
                fade: true,
                speed: 300,
                cssEase: 'linear',
                slidesToShow: 1,
                slidesToScroll: 1,
                draggable: false
            });
        }
        if ($('.slider-services').length) {
            $('.slider-services').slick({
                dots: false,
                infinite: true,
                speed: 300,
                cssEase: 'linear',
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [{
                        breakpoint: 960,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            infinite: true
                        }
                    },
                    {
                        breakpoint: 640,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                            dots: true
                        }
                    }
                ]
            });
        }
        if ($('.slider-gallery').length) {
            $('.slider-gallery').slick({
                dots: false,
                arrows: true,
                infinite: true,
                variableWidth: true,
                speed: 300,
                cssEase: 'linear',
                slidesToShow: 2,
                slidesToScroll: 1,
                responsive: [{
                        breakpoint: 1280,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            variableWidth: true,
                            infinite: true
                        }
                    },
                    {
                        breakpoint: 560,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            variableWidth: false,
                            infinite: true,
                            adaptiveHeight: true
                        }
                    }
                ]
            });
        }
        if ($('.slider-gallery__base').length) {
            $('.slider-gallery__base').slick({
                dots: false,
                arrows: true,
                infinite: true,
                speed: 300,
                cssEase: 'linear',
                slidesToShow: 2,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        variableWidth: false,
                        infinite: true,
                        dots: true,
                        arrows: false
                    }
                }]
            });
        }
        if ($('.slide-menu').length) {
            var $numberReviewmenu = $('.slider-menu__pages .number-review-slide');
            var $slickReviewmenu = $('.slide-menu');
            $slickReviewmenu.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
                var i = (currentSlide ? currentSlide : 0) + 1;
                $numberReviewmenu.html('<span class="curr-number">' + i + '</span> <span class="curr-number_arr"> / </span> ' + 4);
            });
            $('.slide-menu').slick({
                dots: false,
                arrows: true,
                infinite: false,
                fade: true,
                slidesToShow: 1,
                rows: 3,
                slidesPerRow: 1,
                speed: 300,
                cssEase: 'linear',
                //slidesToShow: 2,
                slidesToScroll: 1
            });
        };

    };
    $('body').removeClass('loaded');
    //sliders arrows
    $('.js-arrow-prev').click(function() {
        if ($(this).hasClass('location-arrows__prev')) {
            if ($(window).width() < 992) {
                $('html, body').animate({
                    scrollTop: $('.section-location').offset().top
                }, 1000);
            } else {
                $('html, body').animate({
                    scrollTop: $('.section-location').offset().top - 75
                }, 1000);
            }

            $(this).parents('.slider-location__item').removeClass('location-item__showed').find('.location-item__text').stop().css('max-height', '40px');
            $(this).parents('.slider-location__item').find('.location-item__more').removeClass('active').find('.text__hide').hide().siblings('.text__show').fadeIn();
        }
        $(this).parents(".js-slider").slick('slickPrev');
    });
    $('.js-arrow-next').click(function() {
        if ($(this).hasClass('location-arrows__next')) {
            if ($(window).width() < 992) {
                $('html, body').animate({
                    scrollTop: $('.section-location').offset().top
                }, 1000);
            } else {
                $('html, body').animate({
                    scrollTop: $('.section-location').offset().top - 75
                }, 1000);
            }
            $(this).parents('.slider-location__item').removeClass('location-item__showed').find('.location-item__text').stop().css('max-height', '40px');
            $(this).parents('.slider-location__item').find('.location-item__more').removeClass('active').find('.text__hide').hide().siblings('.text__show').fadeIn();
        }
        $(this).parents(".js-slider").slick('slickNext');
    });


    //popup open
    function heightItem() {
        $('.gallery-cell__tall').each(function() {
            var el = $(this),
                parents = el.closest('.slider-gallery__item');
            el.css('height', parents.outerHeight() * 0.6375)
            el.css('height', parents.outerHeight() * 0.6375)
        });
        $('.gallery-cell__low').each(function() {
            var el = $(this),
                parents = el.closest('.slider-gallery__item');
            el.css('height', parents.outerHeight() * 0.3625)
        });
    }
    //popup open
    $('.js-popup-promo').click(function(e) {
        e.preventDefault();
        $('.pp-bg, .pp-pos').fadeIn();
        if ($('.pp-pos').find('.js-slider').length) {
            $('.pp-pos').find('.js-slider').slick('setPosition');
        }        
        $('body').addClass('bodyoverflow');
        heightItem();
    });
    $(window).resize(function() {
        heightItem();
    });

    if ($('.js-scroll').length) {
        $('.seats-table').mCustomScrollbar({
            axis: "x",
            mouseWheel: {
                preventDefault: true
            },
            documentTouchScroll: true,
            contentTouchScroll: true,
            advanced:{autoExpandVerticalScroll:true},
            mouseWheel:true
        });
    }


    $('.btn-book').click(function(e) {
        $('html, body').animate({
            scrollTop: $('.form-meetings').offset().top - 95
        }, 1000);
        return false;
    });


    // Services - full info 
    $('.js-service-info').mouseleave(function(e) {

        if ($(window).width() > 420) {
            $(this).find('.services-info__text').stop().animate({
                height: "48px"
            }, 750, function() {
                $(this).find('.ellip-line').css('white-space', 'normal');
            });

        } else {
            $(this).find('.services-info__text').stop().animate({
                height: "64px"
            }, 750, function() {
                $(this).find('.ellip-line').css('white-space', 'normal');
            });

        }

    });
    $('.js-service-info').mouseenter(function(e) {
        if (!$('body').is('.ios')) {
            var animtext = $(this).find('.services-info__text');
            $(this).find('.ellip-line').css('white-space', 'normal');
            autoHeightAnimate(animtext, 450);
        }
    });
    $('.js-service-info').click(function(e) {
        if ($('body').is('.ios')) {
            var animtext = $(this).find('.services-info__text');
            $(this).toggleClass('open');
            if ($(this).is('.open') ){
                $(this).find('.ellip-line').css('white-space', 'normal');
                autoHeightAnimate(animtext, 450);
            } else {
                if ($(window).width() > 420) {
                    $(this).find('.services-info__text').stop().animate({
                        height: "48px"
                    }, 750, function() {
                        $(this).find('.ellip-line').css('white-space', 'normal');
                    });

                } else {
                    $(this).find('.services-info__text').stop().animate({
                        height: "64px"
                    }, 750, function() {
                        $(this).find('.ellip-line').css('white-space', 'normal');
                    });

                }
            }
        }
    });

    //scroll-top
    $(".js-scroll-top").click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
        return false;
    });

    if ($('.services-info__text').length) {
        // Services - full info 
        $('.services-info__text').ellipsis({
            lines: 3,
            responsive: true
        });
    }



    // menu
    $('.mobile-nav').click(function(e) {
        e.stopPropagation();
        $(this).toggleClass('active');
        $('.header-nav').slideToggle();
    });
    $('.header-nav').click(function(e) {
        e.stopPropagation();
    });


    $(document).click(function() {
        if ($(window).width() < 1251) {
            $('.mobile-nav').removeClass('active');
            $('.header-nav').slideUp();
        }
    });



    // headings parallax
    if ($('.js-parallax').length) {
        $('.js-parallax').parallax("100%", 0.1);
    }


    //popup close
    $('.pp-close, .pp-bg').click(function() {
        $('.pp-bg, .pp-pos').fadeOut();
        $('body').removeClass('bodyoverflow');
    });
    // show more rows
    $('.button-see-more').click(function(e) {
        e.preventDefault();
        $('.hidden-row').slideDown();
        $(this).hide();
    });
    // location info
    $('.js-location__showmore').click(function(e) {
        e.preventDefault();
        $(this).parents('.slider-location__item').toggleClass('location-item__showed');
        if ($(this).parents('.slider-location__item').hasClass('location-item__showed')) {
            var animtext = $(this).prev('.location-item__text');
            autoHeightAnimate(animtext, 450);
            $(this).addClass('active').find('.text__show').hide().siblings('.text__hide').fadeIn();

        } else {
            if ($(window).width() > 420) {
                $(this).prev('.location-item__text').stop().animate({
                    height: "40px"
                }, 450);
                $(this).removeClass('active').find('.text__hide').hide().siblings('.text__show').fadeIn();
            } else {
                $(this).prev('.location-item__text').stop().animate({
                    height: "60px"
                }, 450);
                $(this).removeClass('active').find('.text__hide').hide().siblings('.text__show').fadeIn();

            }
        }
    });
    /* Function to animate height: auto */
    function autoHeightAnimate(element, time) {
        var curHeight = element.height(), // Get Default Height
            autoHeight = element.css('height', 'auto').height(); // Get Auto Height
        element.height(curHeight); // Reset to Default Height
        element.stop().animate({
            height: autoHeight
        }, time); // Animate to Auto Height
    }
    //style select
    if ($('.js-styled').length) {
        $('select').each(function() {
            $('.js-styled').styler();
        });
        $(".jq-selectbox ul").each(function() {
            $(this).mCustomScrollbar({
                documentTouchScroll: true
            });
        });
    };

    // mask phone
    if ($('.js-mask').length) {
        $('.js-mask').mask('+ 7 (999) 999-99-99');
    }


    $(window).load(function() {
        $('.js-scroll').each(function() {
            var el = $(this);
            if (window.innerWidth < 992 && $('.mCustomScrollbar').length) {
                el.mCustomScrollbar('destroy');
            } else {
                if (!el.is('.mCustomScrollbar')) {
                    el.mCustomScrollbar({
                        mouseWheel: true,
                        documentTouchScroll: true,
                        contentTouchScroll: true
                    });
                }
            }
        });
    });

    // datepick 
    if ($('.js-datepick').length) {
        // ru lang
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '&#x3c;Пред',
            nextText: 'След&#x3e;',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
            ],
            monthNamesShort: ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня',
                'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'
            ],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Нед',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        // datepick     
        var dateToday = new Date();
        var dates = $('.js-datepick').datepicker($.extend({
                dateFormat: "d M yy",
                showOtherMonths: true,
                selectOtherMonths: true,
                minDate: 0,
                defaultDate: +1,
                // return to fields
                onSelect: function(selectedDate) {
                    var option = this.id == "dateStart" ? "minDate" : "maxDate",
                        instance = $(this).data("datepicker"),
                        date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                }
            },
            $.datepicker.regional['ru']
        ));
        // $(".js-datepick").datepicker('setDate', new Date());

    };


    // MAP START
    // maps small
    if ($('#map1').length) {
        var markerIcon = {
            url: 'img/map_marker.png?ver1.2',
        }
        var mapOptions = {
            center: {
                lat: -34.397,
                lng: 150.644
            },
            zoom: 8,
            scrollwheel: false,
            mapTypeControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_BOTTOM
            },
            scaleControl: true,
            streetViewControl: false,
            fullscreenControl: false
        };
        var mapElement = document.getElementById('map1');
        var map = new google.maps.Map(mapElement, mapOptions);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(-34.397, 150.644),
            map: map,
            icon: markerIcon
        });
    }
    if ($('#map2').length) {
        var markerIcon = {
            url: 'img/map_marker.png?ver1.2',
        }
        var mapOptions = {
            center: {
                lat: 43.6018244,
                lng: 39.6550884
            },
            zoom: 8,
            scrollwheel: false,
            mapTypeControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_BOTTOM
            },
            scaleControl: true,
            streetViewControl: false,
            fullscreenControl: false
        };
        var mapElement = document.getElementById('map2');
        var map = new google.maps.Map(mapElement, mapOptions);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(43.6018244, 39.6550884),
            map: map,
            icon: markerIcon
        });
    }
    if ($('#map3').length) {
        var markerIcon = {
            url: 'img/map_marker.png?ver1.2',
        }
        var mapOptions = {
            center: {
                lat: -34.397,
                lng: 150.644
            },
            zoom: 8,
            scrollwheel: false,
            mapTypeControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_BOTTOM
            },
            scaleControl: true,
            streetViewControl: false,
            fullscreenControl: false
        };
        var mapElement = document.getElementById('map3');
        var map = new google.maps.Map(mapElement, mapOptions);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(-34.397, 150.644),
            map: map,
            icon: markerIcon
        });
    }
    if ($('#map4').length) {
        var markerIcon = {
            url: 'img/map_marker.png?ver1.2',
        }
        var mapOptions = {
            center: {
                lat: -34.397,
                lng: 150.644
            },
            zoom: 8,
            scrollwheel: false,
            mapTypeControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_BOTTOM
            },
            scaleControl: true,
            streetViewControl: false,
            fullscreenControl: false
        };
        var mapElement = document.getElementById('map4');
        var map = new google.maps.Map(mapElement, mapOptions);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(-34.397, 150.644),
            map: map,
            icon: markerIcon
        });
    }

    // MAP LOCATION
    if ($('#locationmap')[0]) {

        google.maps.event.addDomListener(window, 'load', init);
        google.maps.event.addDomListener(window, "resize", function init() {});

        var markerIcon = {
            url: 'img/map_marker.png?ver1.2'
        }
        var markerIcon2 = {
            url: 'img/map_marker2.png'
        }

        function init() {
            var mapOptions = {
                zoom: 7,
                center: new google.maps.LatLng(-33.736969, 148.796008),
                mapTypeControl: false,
                zoomControl: true,
                scrollwheel: false,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                },
                scaleControl: true,
                streetViewControl: false,
                fullscreenControl: false
            };
            var mapElement = document.getElementById('locationmap');

            var map = new google.maps.Map(mapElement, mapOptions);

            var infobox1 = new InfoBox({
                content: document.getElementById("infobox1"),
                disableAutoPan: false,
                alignBottom: true,
                pixelOffset: new google.maps.Size(-153, -47),
                zIndex: null,
                boxStyle: {
                    opacity: 1,
                    zIndex: 999,
                },
                closeBoxMargin: "0px",
                closeBoxURL: "",
                infoBoxClearance: new google.maps.Size(1, 1)
            });

            var infobox2 = new InfoBox({
                content: document.getElementById("infobox2"),
                disableAutoPan: false,
                alignBottom: true,
                pixelOffset: new google.maps.Size(-153, -47),
                zIndex: null,
                boxStyle: {
                    opacity: 1,
                    zIndex: 999,
                },
                closeBoxMargin: "0px",
                closeBoxURL: "",
                infoBoxClearance: new google.maps.Size(1, 1)
            });

            var infobox3 = new InfoBox({
                content: document.getElementById("infobox3"),
                disableAutoPan: false,
                alignBottom: true,
                pixelOffset: new google.maps.Size(-153, -47),
                zIndex: null,
                boxStyle: {
                    opacity: 1,
                    zIndex: 999,
                },
                closeBoxMargin: "0px",
                closeBoxURL: "",
                infoBoxClearance: new google.maps.Size(1, 1)
            });

            var infobox4 = new InfoBox({
                content: document.getElementById("infobox4"),
                disableAutoPan: false,
                alignBottom: true,
                pixelOffset: new google.maps.Size(-153, -47),
                zIndex: null,
                boxStyle: {
                    opacity: 1,
                    zIndex: 999,
                },
                closeBoxMargin: "0px",
                closeBoxURL: "",
                infoBoxClearance: new google.maps.Size(1, 1)
            });

            var infobox5 = new InfoBox({
                content: document.getElementById("infobox5"),
                disableAutoPan: false,
                alignBottom: true,
                pixelOffset: new google.maps.Size(-153, -47),
                zIndex: null,
                boxStyle: {
                    opacity: 1,
                    zIndex: 999,
                },
                closeBoxMargin: "0px",
                closeBoxURL: "",
                infoBoxClearance: new google.maps.Size(1, 1)
            });

            google.maps.event.addListener(map, 'click', function() {
                infobox1.close();
            });
            google.maps.event.addListener(map, 'click', function() {
                infobox2.close();
            });
            google.maps.event.addListener(map, 'click', function() {
                infobox3.close();
            });
            google.maps.event.addListener(map, 'click', function() {
                infobox4.close();
            });
            google.maps.event.addListener(map, 'click', function() {
                infobox5.close();
            });

            var marker1 = new google.maps.Marker({
                position: new google.maps.LatLng(-34.481472, 150.409651),
                map: map,
                icon: markerIcon
            });
            marker1.addListener('mouseover', function() {
                infobox1.open(map, this);
            });
            marker1.addListener('mouseout', function() {
                //  infobox1.close(map, this);
            });
            var oldDraw = infobox1.draw;
            infobox1.draw = function() {
                oldDraw.apply(this);
                jQuery(infobox1.div_).hide();
                jQuery(infobox2.div_).hide();
                jQuery(infobox3.div_).hide();
                jQuery(infobox4.div_).hide();
                jQuery(infobox5.div_).hide();
                jQuery(infobox1.div_).fadeIn();
            }

            var marker2 = new google.maps.Marker({
                position: new google.maps.LatLng(-32.927609, 151.775519),
                map: map,
                icon: markerIcon2
            });
            marker2.addListener('mouseover', function() {
                infobox2.open(map, this);
            });
            marker2.addListener('mouseout', function() {
                //  infobox2.close(map, this);
            });
            var oldDraw = infobox1.draw;
            infobox2.draw = function() {
                oldDraw.apply(this);
                jQuery(infobox1.div_).hide();
                jQuery(infobox2.div_).hide();
                jQuery(infobox3.div_).hide();
                jQuery(infobox4.div_).hide();
                jQuery(infobox5.div_).hide();
                jQuery(infobox2.div_).fadeIn();
            }

            var marker3 = new google.maps.Marker({
                position: new google.maps.LatLng(-35.099589, 147.359571),
                map: map,
                icon: markerIcon2
            });
            marker3.addListener('mouseover', function() {
                infobox3.open(map, this);
            });
            marker3.addListener('mouseout', function() {
                //      infobox3.close(map, this);
            });
            var oldDraw = infobox1.draw;
            infobox3.draw = function() {
                oldDraw.apply(this);
                jQuery(infobox1.div_).hide();
                jQuery(infobox2.div_).hide();
                jQuery(infobox3.div_).hide();
                jQuery(infobox4.div_).hide();
                jQuery(infobox5.div_).hide();
                jQuery(infobox3.div_).fadeIn();
            }

            var marker4 = new google.maps.Marker({
                position: new google.maps.LatLng(-34.255236, 146.021974),
                map: map,
                icon: markerIcon2
            });
            marker4.addListener('mouseover', function() {
                infobox4.open(map, this);
            });
            marker4.addListener('mouseout', function() {
                //  infobox4.close(map, this);
            });
            var oldDraw = infobox4.draw;
            infobox4.draw = function() {
                oldDraw.apply(this);
                jQuery(infobox1.div_).hide();
                jQuery(infobox2.div_).hide();
                jQuery(infobox3.div_).hide();
                jQuery(infobox4.div_).hide();
                jQuery(infobox5.div_).hide();
                jQuery(infobox4.div_).fadeIn();
            }

            var marker5 = new google.maps.Marker({
                position: new google.maps.LatLng(-32.228841, 148.644102),
                map: map,
                icon: markerIcon2
            });
            marker5.addListener('mouseover', function() {
                infobox5.open(map, this);
            });
            marker5.addListener('mouseout', function() {
                //      infobox5.close(map, this);
            });
            var oldDraw = infobox5.draw;
            infobox5.draw = function() {
                oldDraw.apply(this);
                jQuery(infobox1.div_).hide();
                jQuery(infobox2.div_).hide();
                jQuery(infobox3.div_).hide();
                jQuery(infobox4.div_).hide();
                jQuery(infobox5.div_).hide();
                jQuery(infobox5.div_).fadeIn();
            }

        }

    }
    // MAP CONTACTS
    if ($('#contactsmap')[0]) {

        google.maps.event.addDomListener(window, 'load', init);
        google.maps.event.addDomListener(window, "resize", function init() {});

        var markerIcon = {
            url: 'img/map_marker.png?ver1.2'
        }

        function init() {
            var mapOptions = {
                zoom: 7,
                center: new google.maps.LatLng(-33.736969, 148.796008),
                mapTypeControl: false,
                zoomControl: true,
                scrollwheel: false,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                },
                scaleControl: true,
                streetViewControl: false,
                fullscreenControl: false
            };
            var mapElement = document.getElementById('contactsmap');

            var map = new google.maps.Map(mapElement, mapOptions);

            var infobox1 = new InfoBox({
                content: document.getElementById("infobox1"),
                disableAutoPan: false,
                alignBottom: true,
                pixelOffset: new google.maps.Size(-153, -47),
                zIndex: null,
                boxStyle: {
                    opacity: 1,
                    zIndex: 999,
                },
                closeBoxMargin: "0px",
                closeBoxURL: "",
                infoBoxClearance: new google.maps.Size(1, 1)
            });

            google.maps.event.addListener(map, 'click', function() {
                infobox1.close();
            });

            var marker1 = new google.maps.Marker({
                position: new google.maps.LatLng(-34.481472, 150.409651),
                map: map,
                icon: markerIcon
            });
            marker1.addListener('mouseover', function() {
                infobox1.open(map, this);
            });
            marker1.addListener('mouseout', function() {
                //  infobox1.close(map, this);
            });
            var oldDraw = infobox1.draw;
            infobox1.draw = function() {
                oldDraw.apply(this);
                jQuery(infobox1.div_).hide();
                jQuery(infobox1.div_).fadeIn();
            }

        }

    }

    // MAPS END 


    if ($('input, textarea').length) {
        /* placeholder*/
        $('input, textarea').placeholder();
        /* placeholder*/
    }
    $('textarea').each(function() {
        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
    }).on('input', function() {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });

    // copyright year
    var y = new Date();
    $('.copyright-year').text(y.getUTCFullYear());

});